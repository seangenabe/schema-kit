# Schema Kit

Schema Kit allows you to create a meta-schema that can be transformed into
TypeScript, JSON Schema, and more.

## Why?

The Javascript ecosystem got fragmented in terms of schema specifications. Every 
niche—Typescript, PropTypes, Mongoose, GraphQL, JSON Schema—has its own way of 
defining schemas, causing unnecessary **schema duplication**. The aim of this 
project is to define a new schema specification that can be translated to the 
language of all of the other schema specifications.

> Wouldn't this just be another standard to the `xkcd: Standards` of Javascript schemas?

Well yes, but the hope is that it'll be the last one we'll need.

> Why not just use one of those existing standards such as JSON Schema?

The aim is to be a superset of all the other schema specifications and just 
starting from one of them might not entirely suffice.

[More topics on the wiki](-/wikis/home).

## Example

Everything is not yet implemented.

Schema Kit

```ts
const schema = object({
  a: string(),
  b: number()
}, { title: "Sample" })
```

TypeScript code generation

```ts
export interface Sample {
  a: string;
  b: number;
}
```

TypeScript

```ts
type T = ToTypescriptType<typeof schema>
// should be equivalent to { a: string; b: number }
```

PropTypes

```ts
{
  a: PropTypes.string,
  b: PropTypes.number
}
```

Mongoose

```ts
new Schema({
  // Schema Kit types are required by default
  a: { type: String, required: true }, 
  b: { type: Number, required: true }
})
```

GraphQL

```ts
new GraphQLObjectType({
  name: "Sample",
  fields: {
    // Schema Kit required types would be semantically equivalent
    // to non-null.
    a: new GraphQLNonNull(GraphQLString),
    b: new GraphQLNonNull(GraphQLFloat)
  }
})
```

JSON Schema


```ts
{
  type: "object",
  properties: {
    a: { type: "string" },
    b: { type: "number" }
  },
  // Schema Kit types are required by default
  required: ["a", "b"]
}

```

## Optional node

While schema specifications represent nullish types in different ways 
(TypeScript: `?` or `undefined` or `null` or `void`; GraphQL: `!`) Schema Kit
provides the node type `optional`. The aim is to disambiguously represent
optional and required values regardless of the specification.
