# TypeScript code generator for Schema Kit

## About Schema Kit

Schema Kit allows you to create a meta-schema that can be transformed into
TypeScript, JSON Schema, and more.

## Usage

| Schema Kit           | TypeScript
| -------------------- | ----------------------------
| `never()`            | `never`
| `any()`              | `any`
| `buildConst(value)`  | `type Title = value`
| `buildNull()`        | `null`
| `boolean()`          | `boolean`
| `number()`           | `number`
| `integer()`          | `number`
| `string()`           | `string`
| `stringEnum(values)` | `enum Title {}`
| `optional(schema)`   | Union with `undefined`. Optional property if inside an object.
| `object(props)`      | `interface Title {}`
| `array(schema)`      | `T[]`
| `union(schemas)`     | The union of the schemas

```typescript
import { toTypescriptCode } from "@schema-kit/tscodegen"
```

### toTypescriptCode(schema, opts)

```typescript
function toTypescriptCode<O extends BaseSchema>(
  schema: Schema<O>,
  opts: ToTypescriptCodeOptions<O>
): string
```

Converts the input schema into TypeScript code.

Type parameters:
* `O` - User-defined schema types.

Parameters:
* `schema: Schema<O>` - The input schema.
* `opts: ToTypescriptCodeOptions<O> = { prettier: { filepath: "file.ts" } }`
  * `prettier?: prettier.Options` - Options to pass to Prettier
  * `overrideResolvers?(defaultResolvers)` - Override the resolvers used.

Returns: `string` - The output TypeScript code.

Technical limitation: Currently all object and stringEnum schemas generate
interfaces and enums. Interfaces and enums without a title are given a
deterministically-generated ID prefixed with `__`.

#### overrideResolvers

Useful if you want to override the resolvers used to resolve types,
e.g. if you have a custom node type.
The function takes the key-value object of default resolvers.
You must return a similarly-typed object with your own resolvers.
Do not mutate the set of default resolvers!
