import {
  any,
  array,
  boolean,
  buildNull,
  integer,
  never,
  number,
  object,
  optional,
  string,
  stringEnum,
  union,
} from "@schema-kit/common"
import test from "ava"
import { toTypescriptCode } from "./tscodegen"
import dedent = require("dedent")
import { buildConst } from "@schema-kit/common/src/const"

test("integer", (t) => {
  const code = toTypescriptCode(integer({ title: "Foo" }))
  t.is(code, "export type Foo = number;")
})

test("number", (t) => {
  const code = toTypescriptCode(number({ title: "Foo" }))
  t.is(code, "export type Foo = number;")
})

test("string", (t) => {
  const code = toTypescriptCode(string({ title: "Foo" }))
  t.is(code, "export type Foo = string;")
})

test("boolean", (t) => {
  const code = toTypescriptCode(boolean({ title: "Foo" }))
  t.is(code, "export type Foo = boolean;")
})

test("null", (t) => {
  const code = toTypescriptCode(buildNull({ title: "Foo" }))
  t.is(code, "export type Foo = null;")
})

test("object", (t) => {
  const code = toTypescriptCode(
    object({ a: number(), b: string() }, { title: "Foo" })
  )
  const expected = dedent`
    export interface Foo {
      a: number;
      b: string;
    }`
  t.is(code, expected)
})

test("object escaped keys", (t) => {
  const code = toTypescriptCode(
    object({ "colon:": string() }, { title: "Foo" })
  )
  const expected = dedent`
    export interface Foo {
      "colon:": string;
    }`
  t.is(code, expected)
})

test("nested object title", (t) => {
  const code = toTypescriptCode(
    object({ a: object({ b: string() }, { title: "Bar" }) }, { title: "Foo" })
  )
  const expected = dedent`
    export interface Bar {
      b: string;
    }

    export interface Foo {
      a: Bar;
    }
  `
  t.is(code, expected)
})

test("optional", (t) => {
  const code = toTypescriptCode(
    object({ a: optional(string()) }, { title: "Foo" })
  )
  const expected = dedent`
    export interface Foo {
      a?: string;
    }`
  t.is(code, expected)
})

test("ref equality", (t) => {
  const bar = object({ c: string() })
  const foo = object({ a: bar, b: optional(bar) }, { title: "Foo" })
  const expected = dedent`
    interface __0 {
      c: string;
    }

    export interface Foo {
      a: __0;
      b?: __0;
    }
    `
  const code = toTypescriptCode(foo)
  t.is(code, expected)
})

test("description as JSDoc", (t) => {
  const code = toTypescriptCode(
    number({ title: "Foo", description: "A cool number." })
  )
  const expected = dedent`
    /** A cool number. */
    export type Foo = number;`
  t.is(code, expected)
})

test("property description as JSDoc", (t) => {
  const code = toTypescriptCode(
    object({ a: number({ description: "A cool number." }) }, { title: "Foo" })
  )
  const expected = dedent`
    export interface Foo {
      /** A cool number. */
      a: number;
    }`
  t.is(code, expected)
})

test("any", (t) => {
  const code = toTypescriptCode(any({ title: "Foo" }))
  const expected = "export type Foo = any;"
  t.is(code, expected)
})

test("never", (t) => {
  const code = toTypescriptCode(never({ title: "Foo" }))
  const expected = "export type Foo = never;"
  t.is(code, expected)
})

test("array", (t) => {
  const code = toTypescriptCode(array(number(), { title: "Foo" }))
  const expected = "export type Foo = number[];"
  t.is(code, expected)

  const code2 = toTypescriptCode(array(optional(number()), { title: "Bar" }))
  const expected2 = "export type Bar = (number | undefined)[];"
  t.is(code2, expected2)
})

test("stringEnum", (t) => {
  const code = toTypescriptCode(
    stringEnum(["bar", "baz"] as const, { title: "Foo" })
  )
  const expected = dedent`
    export enum Foo {
      bar = "bar",
      baz = "baz",
    }`
  t.is(code, expected)
})

test("stringEnum escaped characters", (t) => {
  const code = toTypescriptCode(
    stringEnum(['dq"', "sq'", "dashed-"] as const, { title: "Foo" })
  )
  const expected = dedent`
    export enum Foo {
      'dq"' = 'dq"',
      "sq'" = "sq'",
      "dashed-" = "dashed-",
    }
  `
  t.is(code, expected)
})

test("stringEnum JSDoc", (t) => {
  const code = toTypescriptCode(
    stringEnum([{ value: "bar", opts: { description: "Bar" } }] as const, {
      title: "Foo",
    })
  )
  const expected = dedent`
    export enum Foo {
      /** Bar */
      bar = "bar",
    }
  `
  t.is(code, expected)
})

test("undefined as const", (t) => {
  const code = toTypescriptCode(buildConst(undefined, { title: "Foo" }))
  const expected = "export type Foo = undefined;"
  t.is(code, expected)
})

test("const", (t) => {
  const f = (value: unknown) =>
    toTypescriptCode(buildConst(value, { title: "Foo" }))
  t.is(f(null), "export type Foo = null;")
  t.is(f(2), "export type Foo = 2;")
  t.is(f("w"), 'export type Foo = "w";')
  t.is(f(true), "export type Foo = true;")
  t.is(f(20n), "export type Foo = 20n;")
})

test("union", (t) => {
  const code = toTypescriptCode(union([string(), number()], { title: "Foo" }))
  t.is(code, "export type Foo = string | number;")
})
