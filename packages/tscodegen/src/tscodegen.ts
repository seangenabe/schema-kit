import { Schema } from "@schema-kit/common"
import { ArrayType } from "@schema-kit/common/src/array"
import { BooleanType } from "@schema-kit/common/src/boolean"
import { BaseSchema } from "@schema-kit/common/src/common"
import { ConstType } from "@schema-kit/common/src/const"
import { ObjectType } from "@schema-kit/common/src/object"
import { OptionalType } from "@schema-kit/common/src/optional"
import { StringEnumType } from "@schema-kit/common/src/string-enum"
import { format, Options } from "prettier"
import {
  OptionalKind,
  Project,
  PropertySignatureStructure,
  SourceFile,
  TypeAliasDeclarationStructure,
} from "ts-morph"
import { UnionType } from "@schema-kit/common/src/union"
import { baseConvert } from "./util/base-convert"
import { toTsIdentifier } from "./util/to-ts-identifier"
import { stringEscape } from "./util/string-escape"

const ALPHABET =
  "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ$_"

function tryTsIdentifier(id: string) {
  try {
    return toTsIdentifier(id)
  } catch (err) {
    return `"${stringEscape(id)}"`
  }
}

export const defaultTypeResolvers = {
  integer() {
    return defaultTypeResolvers.number()
  },
  number() {
    return { type: "number" }
  },
  string() {
    return { type: "string" }
  },
  boolean() {
    return { type: "boolean" }
  },
  null() {
    return { type: "null" }
  },
  any() {
    return { type: "any" }
  },
  never() {
    return { type: "never" }
  },
  optional<O extends BaseSchema = BooleanType>(
    schema: OptionalType<O>,
    { recurse }: { recurse: (schema: Schema<O>) => GetTypeExpressionResult }
  ): GetTypeExpressionResult {
    return {
      isOptional: true,
      type: `(${recurse(schema.child).type} | undefined)`,
    }
  },
  object<O extends BaseSchema = BooleanType>(
    schema: ObjectType<O>,
    {
      recurse,
      sourceFile,
      generateId,
    }: {
      recurse: (schema: Schema<O>) => GetTypeExpressionResult
      sourceFile: SourceFile
      generateId: () => string
    }
  ): GetTypeExpressionResult {
    const id = toTsIdentifier(schema.opts.title ?? generateId())
    sourceFile.addInterface({
      name: id,
      properties: Object.entries(schema.properties).map(([key, child]) => {
        // If the type is optional, bypass getting its type
        // and get its child's type instead.
        let childTypeExpression =
          child.type === "optional"
            ? recurse((child as OptionalType<O, Schema<O>>).child)
            : recurse(child)
        const ret: OptionalKind<PropertySignatureStructure> = {
          name: tryTsIdentifier(key),
          // The type is marked optional here instead.
          hasQuestionToken: child.type === "optional",
          type: childTypeExpression.type,
        }
        if (child.opts.description) {
          ret.docs = [{ description: child.opts.description }]
        }
        return ret
      }),
      isExported: Boolean(schema.opts.title),
    })
    return { type: id }
  },
  array<O extends BaseSchema = BooleanType>(
    schema: ArrayType<O>,
    { recurse }: ResolverOptions<O>
  ): GetTypeExpressionResult {
    const childTypeExpression = recurse(schema.child)
    return { type: `((${childTypeExpression.type})[])` }
  },
  stringEnum<O extends BaseSchema = BooleanType>(
    schema: StringEnumType,
    {
      recurse,
      sourceFile,
      generateId,
    }: {
      recurse: (schema: Schema<O>) => GetTypeExpressionResult
      sourceFile: SourceFile
      generateId: () => string
    }
  ): GetTypeExpressionResult {
    const id = toTsIdentifier(schema.opts.title ?? generateId())

    const enumDeclaration = sourceFile.addEnum({
      name: id,
      isExported: true,
    })

    for (const v of schema.values) {
      const member = enumDeclaration.addMember({
        name: tryTsIdentifier(v.value),
        value: v.value,
      })
      if (v.opts?.description) {
        member.addJsDoc({
          description: v.opts?.description,
        })
      }
    }

    return { type: id }
  },
  const<T = unknown>(schema: ConstType<T>) {
    const { value } = schema

    if (value === undefined) {
      return { type: "undefined" }
    }
    if (
      value === null ||
      typeof value === "number" ||
      typeof value === "string" ||
      typeof value === "boolean"
    ) {
      return { type: JSON.stringify(value) }
    }
    if (typeof value === "bigint") {
      return { type: `${value.toString(10)}n` }
    }
    throw new TypeError("Const value type not supported.")
  },
  union<O extends BaseSchema = BooleanType>(
    schema: UnionType<O>,
    { recurse }: { recurse: (schema: Schema<O>) => GetTypeExpressionResult }
  ) {
    const { children } = schema

    return {
      type: `(${children.map((child) => recurse(child).type).join("|")})`,
    }
  },
} as const

export function toTypescriptCode<O extends BaseSchema = BooleanType>(
  schema: Schema<O>,
  opts: ToTypescriptCodeOptions<O> = { prettier: { filepath: "file.ts" } }
) {
  const project = new Project({ compilerOptions: { declaration: true } })
  const sourceFile = project.createSourceFile("file.ts")
  const refs = new Map<Schema<O>, GetTypeExpressionResult>()

  let id = 0
  const generateId = () => `__${baseConvert(id++, ALPHABET)}`
  const resolvers: Record<string, Resolver<O>> = (
    opts.overrideResolvers ?? ((r) => r)
  )((defaultTypeResolvers as unknown) as Record<string, Resolver<O>>)

  const getType = (schema: Schema<O>): GetTypeExpressionResult => {
    if (refs.has(schema)) return refs.get(schema)!

    const resolver = resolvers[schema.type]
    if (!resolvers[schema.type]) {
      throw new Error("Cannot resolve unknown type.")
    }

    const opts: ResolverOptions<O> = {
      generateId,
      sourceFile,
      recurse: getType,
    }

    const ret = resolver(schema, opts)
    refs.set(schema, ret)
    return ret
  }

  if (!schema.opts.title) {
    throw new Error("Top-level schema must have a title.")
  }
  const topLevelType = getType(schema)
  if (!(schema.type === "object" || schema.type === "stringEnum")) {
    const aliasStructure: OptionalKind<TypeAliasDeclarationStructure> = {
      name: toTsIdentifier(schema.opts.title),
      type: topLevelType.type,
      isExported: true,
    }
    if (schema.opts.description) {
      aliasStructure.docs = [{ description: schema.opts.description }]
    }
    sourceFile.addTypeAlias(aliasStructure)
  }
  sourceFile.formatText({})

  return format(sourceFile.getFullText(), opts.prettier).trim()
}

export interface ToTypescriptCodeOptions<O extends BaseSchema> {
  prettier?: Options
  overrideResolvers?(
    defaultResolvers: Record<string, Resolver<O>>
  ): Record<string, Resolver<O>>
}

interface GetTypeExpressionResult {
  isOptional?: boolean
  type: string
}

export interface ResolverOptions<O extends BaseSchema> {
  recurse(schema: Schema<O>): GetTypeExpressionResult
  sourceFile: SourceFile
  generateId(): string
}

export interface Resolver<O extends BaseSchema> {
  (schema: Schema<O>, opts: ResolverOptions<O>): GetTypeExpressionResult
}

declare module "@schema-kit/common/src/string-enum" {
  export interface StringEnumMemberOptions<T extends string = string> {
    description: string
  }
}
