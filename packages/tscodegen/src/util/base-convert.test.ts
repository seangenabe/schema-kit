import test from "ava"
import { baseConvert } from "./base-convert"

const alphabet = "0123456789abcdef"

test("0", (t) => {
  const result = baseConvert(0, alphabet)
  t.is(result, "0")
})

test("single digit", (t) => {
  const result = baseConvert(15, alphabet)
  t.is(result, "f")
})

test("10_16", (t) => {
  const result = baseConvert(16, alphabet)
  t.is(result, "10")
})

test("1234", (t) => {
  const result = baseConvert(1234, alphabet)
  t.is(result, "4d2")
})
