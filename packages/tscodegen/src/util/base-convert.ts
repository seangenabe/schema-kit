export function baseConvert(n: number, alphabet: string) {
  const len = alphabet.length
  let current = n
  let ret: string[] = []

  do {
    const div = Math.floor(current / len)
    const mod = current % len

    ret.unshift(alphabet[mod])
    current = div
  } while (current !== 0)

  return ret.join("")
}
