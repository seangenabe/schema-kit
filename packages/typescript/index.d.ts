import { AnyType } from "@schema-kit/common/src/any"
import { ArrayType } from "@schema-kit/common/src/array"
import { BooleanType } from "@schema-kit/common/src/boolean"
import { ConstType } from "@schema-kit/common/src/const"
import { IntegerType } from "@schema-kit/common/src/integer"
import { NeverType } from "@schema-kit/common/src/never"
import { NullType } from "@schema-kit/common/src/null"
import { NumberType } from "@schema-kit/common/src/number"
import { ObjectType } from "@schema-kit/common/src/object"
import { OptionalType } from "@schema-kit/common/src/optional"
import { StringType } from "@schema-kit/common/src/string"
import { StringEnumType } from "@schema-kit/common/src/string-enum"
import { UnionType } from "@schema-kit/common/src/union"

export type RequiredKeys<T> = {
  [key in keyof T]: T[key] extends OptionalType ? never : key
}[keyof T]

export type OptionalKeys<T> = {
  [key in keyof T]: T[key] extends OptionalType ? key : never
}[keyof T]

export type ObjectOutType<T> = {
  [key in RequiredKeys<T>]: ToTypescriptType<T[key]>
} &
  {
    [key in OptionalKeys<T>]?: ToTypescriptType<T[key]>
  }

export type ToTypescriptType<S> = S extends NullType
  ? null
  : S extends BooleanType
  ? boolean
  : S extends StringType
  ? string
  : S extends NumberType
  ? number
  : S extends IntegerType
  ? number
  : S extends ConstType<infer ConstT>
  ? ConstT
  : S extends AnyType
  ? any
  : S extends ObjectType<any, infer T>
  ? ObjectOutType<T>
  : S extends ArrayType<any, infer T>
  ? ToTypescriptType<T>[]
  : S extends StringEnumType<infer T>
  ? T[number]
  : S extends UnionType<any, infer T>
  ? { [key in keyof T]: ToTypescriptType<T[key]> }[number]
  : S extends NeverType
  ? never
  : S extends OptionalType<any, infer T>
  ? ToTypescriptType<T> | undefined
  : any
