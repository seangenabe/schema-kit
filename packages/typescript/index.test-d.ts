import {
  any,
  boolean,
  buildNull,
  never,
  number,
  integer,
  string,
  optional,
  object,
  array,
  stringEnum,
  buildConst,
  union,
} from "@schema-kit/common"
import { expectAssignable, expectType } from "tsd"
import { ToTypescriptType } from "."

declare const getType: <T>(schema: T) => ToTypescriptType<T>

const booleanTyped = getType(boolean())
expectType<boolean>(booleanTyped)

const nullTyped = getType(buildNull())
expectType<null>(nullTyped)

const neverTyped = getType(never())
expectType<never>(neverTyped)

expectType<any>(getType(any()))

const stringTyped = getType(string())
expectType<string>(stringTyped)

const numberTyped = getType(number())
expectType<number>(numberTyped)

const integerTyped = getType(integer())
expectType<number>(integerTyped)

const optionalTyped = getType(optional(string()))
expectType<string | undefined>(optionalTyped)

const objectTyped = getType(
  object({
    a: string(),
    b: optional(number()),
  })
)
expectAssignable<{ a: string; b?: number }>(objectTyped)
expectAssignable<typeof objectTyped>({ a: "foo", b: 12 })
expectAssignable<typeof objectTyped>({ a: "foo" })

const arrayTyped = getType(array(string()))
expectType<string[]>(arrayTyped)

const stringEnumTyped = getType(stringEnum(["foo", "bar"] as const))
expectType<"foo" | "bar">(stringEnumTyped)

const stringEnumMemberTyped = getType(
  stringEnum(["bar", { value: "foo" as const }] as const)
)
expectType<"foo" | "bar">(stringEnumMemberTyped)

const constTyped = getType(buildConst("foo" as "foo"))
expectType<"foo">(constTyped)

const unionTyped = getType(union([string(), number()]))
expectType<string | number>(unionTyped)
