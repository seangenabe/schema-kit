# TypeScript type creator for Schema Kit

## About Schema Kit

Schema Kit allows you to create a meta-schema that can be transformed into
TypeScript, JSON Schema, and more.

## Usage

For when you're feeling too lazy to use @schema-kit/tscodegen. 😆

| Schema Kit           | TypeScript
| -------------------- | ----------------------------
| `never()`            | `never`
| `any()`              | `any`
| `buildConst(value)`  | The type of the value
| `buildNull()`        | `null`
| `boolean()`          | `boolean`
| `number()`           | `number`
| `integer()`          | `number`
| `string()`           | `string`
| `stringEnum(values)` | `T[number]`
| `optional(schema)`   | Union with `undefined`
| `object(props)`      | Object with the appropriate optional and required properties
| `array(schema)`      | `T[]`
| `union(schemas)`     | The union of the schemas

```typescript
import { toTypescriptCode } from "@schema-kit/tscodegen"
```

### ToTypescriptType<Schema>

```typescript
type ToTypescriptType<Schema>
```

Converts the type of the input schema into a TypeScript type.

Type parameters:
* `Schema` - The input schema.

Usage:

```typescript
const schema = buildNull()
type NullType = ToTypescriptType<typeof schema>
```
