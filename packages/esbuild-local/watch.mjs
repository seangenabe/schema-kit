#!/usr/bin/env node

import esbuild from "esbuild"
import { watch } from "fs"
import globby from "globby"
import { join } from "path"
import replaceExt from "replace-ext"

const src = `${process.cwd()}/src`

;(async () => {
  try {
    const files = await globby("**/*.ts", {
      ignore: ["node_modules", "**/*.d.ts"],
      cwd: src,
    })
    const watchers = []
    for (const file of files) {
      const realFile = join(src, file)
      watchers.push(
        watch(realFile, async (eventType, filename) => {
          if (eventType === "change") {
            try {
              await esbuild.build({
                entryPoints: [realFile],
                outfile: replaceExt(realFile, ".js"),
                format: "cjs",
                minify: true,
                target: "es2020",
              })
            } catch (err) {
              console.error(err)
            }
          }
        })
      )
    }
    console.log(`Watching ${watchers.length} files.`)
  } catch (err) {
    console.error(err)
    process.exit(1)
  }
})()
