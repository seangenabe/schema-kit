import {
  any,
  array,
  boolean,
  buildConst,
  buildNull,
  integer,
  never,
  number,
  object,
  optional,
  string,
  stringEnum,
  union,
} from "@schema-kit/common"
import { ArrayBuilderOptions } from "@schema-kit/common/src/array"
import { ConstBuilderOptions } from "@schema-kit/common/src/const"
import { ObjectBuilderOptions } from "@schema-kit/common/src/object"
import { StringEnumBuilderOptions } from "@schema-kit/common/src/string-enum"
import test from "ava"
import { toJsonSchema } from "./json-schema"

test("never", (t) => {
  t.is(toJsonSchema(never()), false)
})

test("any", (t) => {
  t.deepEqual(toJsonSchema(any()), {})
})

test("null", (t) => {
  t.deepEqual(toJsonSchema(buildNull()), { type: "null" })
})

test("boolean", (t) => {
  t.deepEqual(toJsonSchema(boolean()), { type: "boolean" })
})

test("number", (t) => {
  t.deepEqual(toJsonSchema(number()), { type: "number" })
})

test("number multipleOf", (t) => {
  t.deepEqual(toJsonSchema(number({ multipleOf: 5 })), {
    type: "number",
    multipleOf: 5,
  })
})

test("number range", (t) => {
  t.deepEqual(toJsonSchema(number({ minimum: 1, maximum: 5 })), {
    type: "number",
    minimum: 1,
    maximum: 5,
  })
  t.deepEqual(
    toJsonSchema(number({ exclusiveMinimum: -1, exclusiveMaximum: 1 })),
    {
      type: "number",
      exclusiveMinimum: -1,
      exclusiveMaximum: 1,
    }
  )
})

test("integer", (t) => {
  t.deepEqual(toJsonSchema(integer()), { type: "integer" })
})

test("string", (t) => {
  t.deepEqual(toJsonSchema(string()), { type: "string" })
})

test("string length", (t) => {
  t.deepEqual(toJsonSchema(string({ minLength: 2 })), {
    type: "string",
    minLength: 2,
  })
  t.deepEqual(toJsonSchema(string({ maxLength: 9 })), {
    type: "string",
    maxLength: 9,
  })
})

test("string pattern", (t) => {
  t.deepEqual(toJsonSchema(string({ pattern: "." })), {
    type: "string",
    pattern: ".",
  })
})

test("string format", (t) => {
  t.deepEqual(toJsonSchema(string({ format: "date" })), {
    type: "string",
    format: "date",
  })
})

test("object", (t) => {
  t.like(toJsonSchema(object({ a: string(), b: optional(number()) })), {
    type: "object",
    properties: { a: { type: "string" }, b: { type: "number" } },
    required: ["a"],
  })
})

test("array", (t) => {
  t.deepEqual(toJsonSchema(array(string())), {
    type: "array",
    items: { type: "string" },
  })
})

test("array length", (t) => {
  t.deepEqual(toJsonSchema(array(string(), { minLength: 1, maxLength: 5 })), {
    type: "array",
    items: { type: "string" },
    minLength: 1,
    maxLength: 5,
  })
})

test("array uniqueness", (t) => {
  t.deepEqual(toJsonSchema(array(string(), { uniqueItems: true })), {
    type: "array",
    items: { type: "string" },
    uniqueItems: true,
  })
})

test("stringEnum", (t) => {
  t.like(toJsonSchema(stringEnum(["bar", "baz"])), {
    enum: ["bar", "baz"],
  })
})

test("const", (t) => {
  const f = <T>(value: T) => toJsonSchema(buildConst<T>(value))
  t.like(f(null), { const: null })
  t.like(f(1), { const: 1 })
  t.like(f("bar"), { const: "bar" })
  t.like(f(true), { const: true })
})

test("generic properties", (t) => {
  const testData: [(opts: any) => unknown, string | {}][] = [
    [buildNull, "null"],
    [any, {}],
    [boolean, "boolean"],
    [number, "number"],
    [integer, "integer"],
    [string, "string"],
    [(opts: ObjectBuilderOptions) => object({}, opts), "object"],
    [(opts: ArrayBuilderOptions) => array(any(), opts), "array"],
    [
      (opts: StringEnumBuilderOptions) => stringEnum(["a"], opts),
      { enum: ["a"] },
    ],
    [(opts: ConstBuilderOptions) => buildConst(1, opts), { const: 1 }],
  ]
  for (const [fn, shape] of testData) {
    const expected =
      typeof shape === "string"
        ? { type: shape, title: "Foo", description: "s" }
        : { ...shape, title: "Foo", description: "s" }
    const actual = toJsonSchema(fn({ title: "Foo", description: "s" }) as any)
    t.like(actual, expected)
    if (fn === any) {
      t.false("type" in (actual as {}))
    }
  }
  t.deepEqual(
    toJsonSchema(string({ default: "bar", examples: ["bar", "burger"] })),
    {
      type: "string",
      default: "bar",
      examples: ["bar", "burger"],
    }
  )
})

test("anyOf", (t) => {
  t.deepEqual(toJsonSchema(union([string(), number()])), {
    anyOf: [{ type: "string" }, { type: "number" }],
  })
})
