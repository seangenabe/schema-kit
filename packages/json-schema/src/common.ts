// TypeScript erroneously marks these imports as unused
// @ts-nocheck
import { AnyBuilderOptions } from "@schema-kit/common/src/any"
import { ArrayBuilderOptions } from "@schema-kit/common/src/array"
import { BooleanBuilderOptions } from "@schema-kit/common/src/boolean"
import { ConstBuilderOptions } from "@schema-kit/common/src/const"
import { IntegerBuilderOptions } from "@schema-kit/common/src/integer"
import { NullBuilderOptions } from "@schema-kit/common/src/null"
import { NumberBuilderOptions } from "@schema-kit/common/src/number"
import { ObjectBuilderOptions } from "@schema-kit/common/src/object"
import { StringBuilderOptions } from "@schema-kit/common/src/string"
import { StringEnumBuilderOptions } from "@schema-kit/common/src/string-enum"
import {
  ArrayJsonSchemaExclusive,
  JsonSchemaGeneric,
  NumericJsonSchemaExclusive,
  StringJsonSchemaExclusive,
} from "./json-schema"

declare module "@schema-kit/common/src/string" {
  export interface StringBuilderOptions
    extends StringJsonSchemaExclusive,
      JsonSchemaGeneric<string> {}
}

declare module "@schema-kit/common/src/integer" {
  export interface IntegerBuilderOptions
    extends NumericJsonSchemaExclusive,
      JsonSchemaGeneric<number> {}
}

declare module "@schema-kit/common/src/number" {
  export interface NumberBuilderOptions
    extends NumericJsonSchemaExclusive,
      JsonSchemaGeneric<number> {}
}

declare module "@schema-kit/common/src/array" {
  export interface ArrayBuilderOptions
    extends ArrayJsonSchemaExclusive,
      JsonSchemaGeneric<unknown[]> {}
}

declare module "@schema-kit/common/src/any" {
  export interface AnyBuilderOptions extends JsonSchemaGeneric<unknown> {}
}

declare module "@schema-kit/common/src/boolean" {
  export interface BooleanBuilderOptions extends JsonSchemaGeneric<boolean> {}
}

declare module "@schema-kit/common/src/const" {
  export interface ConstBuilderOptions extends JsonSchemaGeneric<unknown> {}
}

declare module "@schema-kit/common/src/null" {
  export interface NullBuilderOptions extends JsonSchemaGeneric<null> {}
}

declare module "@schema-kit/common/src/object" {
  export interface ObjectBuilderOptions extends JsonSchemaGeneric<{}> {}
}

declare module "@schema-kit/common/src/optional" {
  export interface OptionalBuilderOptions extends JsonSchemaGeneric<unknown> {}
}

declare module "@schema-kit/common/src/string-enum" {
  export interface StringEnumBuilderOptions extends JsonSchemaGeneric<string> {}
}
