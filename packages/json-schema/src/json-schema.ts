import { BaseSchema, CommonBuilderOptions, Schema } from "@schema-kit/common"
import { AnyType } from "@schema-kit/common/src/any"
import { ArrayType } from "@schema-kit/common/src/array"
import { BooleanType } from "@schema-kit/common/src/boolean"
import { ConstType } from "@schema-kit/common/src/const"
import { IntegerType } from "@schema-kit/common/src/integer"
import { NeverType } from "@schema-kit/common/src/never"
import { NullType } from "@schema-kit/common/src/null"
import { NumberType } from "@schema-kit/common/src/number"
import { ObjectType } from "@schema-kit/common/src/object"
import { OptionalType } from "@schema-kit/common/src/optional"
import { StringType } from "@schema-kit/common/src/string"
import { StringEnumType } from "@schema-kit/common/src/string-enum"
import { UnionType } from "@schema-kit/common/src/union"
import "./common"

const OPTIONAL_MARKER: unique symbol = Symbol()

export const defaultResolvers = {
  never() {
    return false
  },
  any(schema: AnyType) {
    return mergePropertiesToJsonSchema<CommonBuilderOptions>({}, schema.opts)
  },
  null(schema: NullType) {
    return mergePropertiesToJsonSchema<CommonBuilderOptions>(
      { type: "null" } as NullJsonSchema,
      schema.opts
    )
  },
  boolean(schema: BooleanType) {
    return mergePropertiesToJsonSchema<CommonBuilderOptions>(
      { type: "boolean" } as BooleanJsonSchema,
      schema.opts
    )
  },
  number(schema: NumberType) {
    return mergePropertiesToJsonSchema<NumericJsonSchemaExclusive>(
      { type: "number" } as NumberJsonSchema,
      schema.opts,
      [
        "multipleOf",
        "minimum",
        "maximum",
        "exclusiveMinimum",
        "exclusiveMaximum",
      ]
    )
  },
  integer(schema: IntegerType) {
    return mergePropertiesToJsonSchema<NumericJsonSchemaExclusive>(
      { type: "integer" } as IntegerJsonSchema,
      schema.opts,
      [
        "multipleOf",
        "minimum",
        "maximum",
        "exclusiveMinimum",
        "exclusiveMaximum",
      ]
    )
  },
  string(schema: StringType) {
    return mergePropertiesToJsonSchema<StringJsonSchemaExclusive>(
      { type: "string" } as StringJsonSchema,
      schema.opts,
      ["minLength", "maxLength", "pattern", "format"]
    )
  },
  optional(schema: OptionalType) {
    const ret = toJsonSchema(schema.child) as {}

    if (ret === false) return false
    const ret2 = { ...ret }
    Object.defineProperty(ret2, OPTIONAL_MARKER, {
      value: true,
    })

    return mergePropertiesToJsonSchema(ret2, schema.opts)
  },
  object<O extends BaseSchema = BooleanType>(schema: ObjectType<O>) {
    const properties: Record<string, unknown> = {}
    const required: string[] = []

    for (const [key, value] of Object.entries(schema.properties)) {
      const valueSchema = toJsonSchema(value)
      properties[key] = valueSchema
      if (valueSchema && !(valueSchema as {})[OPTIONAL_MARKER]) {
        required.push(key)
      }
    }

    const ret: ObjectJsonSchema = {
      type: "object",
      properties,
    }
    if (required.length !== 0) {
      ret.required = required
    }
    return mergePropertiesToJsonSchema<{}>(ret, schema.opts)
  },
  stringEnum<T extends string[]>(schema: StringEnumType<T>) {
    return mergePropertiesToJsonSchema<{}>(
      {
        type: "string",
        enum: schema.values.map((v) => v.value) as unknown,
      } as StringEnumJsonSchema<T>,
      schema.opts
    )
  },
  const<T>(schema: ConstType<T>) {
    const { value } = schema
    const ret = { const: value } as ConstJsonSchema<T>
    if (value === null) {
      ret.type = "null"
    } else if (typeof value === "boolean") {
      ret.type = "boolean"
    } else if (typeof value === "number") {
      ret.type = "number"
    } else if (typeof value === "string") {
      ret.type = "string"
    } else if (Array.isArray(value)) {
      ret.type = "array"
    }

    return mergePropertiesToJsonSchema(ret, schema.opts)
  },
  array<T>(schema: ArrayType) {
    return mergePropertiesToJsonSchema<ArrayJsonSchemaExclusive>(
      {
        type: "array",
        items: toJsonSchema(schema.child),
      } as ArrayJsonSchema<T>,
      schema.opts,
      ["minLength", "maxLength", "uniqueItems"]
    )
  },
  union(schema: UnionType) {
    return mergePropertiesToJsonSchema<{}>(
      {
        anyOf: schema.children.map((child) => toJsonSchema(child)),
      } as AnyOfJsonSchema,
      schema.opts
    )
  },
}

export function toJsonSchema<T>(schema: ConstType<T>): ConstJsonSchema<T>
export function toJsonSchema(schema: NeverType): false
export function toJsonSchema(schema: AnyType): {}
export function toJsonSchema(schema: NullType): NullJsonSchema
export function toJsonSchema(schema: BooleanType): BooleanJsonSchema
export function toJsonSchema(schema: NumberType): NumberJsonSchema
export function toJsonSchema(schema: IntegerType): IntegerJsonSchema
export function toJsonSchema(schema: StringType): StringJsonSchema
export function toJsonSchema<T extends BaseSchema = BooleanType>(
  schema: OptionalType<T>
): unknown
export function toJsonSchema<
  O extends BaseSchema = BooleanType,
  T extends Record<string, Schema<O>> = Record<string, Schema<O>>
>(schema: ObjectType<O, T>): ObjectJsonSchema<O, T>
export function toJsonSchema<O extends BaseSchema = BooleanType>(
  schema: Schema<O>
): unknown
export function toJsonSchema<
  O extends BaseSchema = BooleanType,
  T extends Schema<O> = Schema<O>
>(schema: ArrayType<O>): ArrayJsonSchema<T>
export function toJsonSchema<T extends readonly string[] = readonly string[]>(
  schema: StringEnumType<T>
): StringEnumJsonSchema<T>
export function toJsonSchema(schema: UnionType<any, any>): AnyOfJsonSchema

export function toJsonSchema(schema: Schema<BaseSchema>): unknown {
  const resolvers = defaultResolvers

  const resolver = resolvers[schema.type]

  if (!resolver) {
    throw new Error("Schema type not supported.")
  }
  return resolver(schema)
}

export type TypeFromSchemaKit<T extends BaseSchema> = T extends NeverType
  ? false
  : never

export interface JsonSchemaGeneric<T = unknown> {
  title?: string
  description?: string
  default?: T
  examples?: T[]
  raw?: Record<string, unknown>
}

export interface NullJsonSchema extends JsonSchemaGeneric<null> {
  type: "null"
}

export interface BooleanJsonSchema extends JsonSchemaGeneric<boolean> {
  type: "boolean"
}

export interface ObjectJsonSchema<
  O extends BaseSchema = BooleanType,
  Props extends Record<string, Schema<O>> = Record<string, Schema<O>>
> extends JsonSchemaGeneric<{}> {
  type: "object"
  properties?: {
    [key in keyof Props]: ValueJsonSchema<Props[key]>
  }
  required?: (keyof Props)[]
}

export interface ArrayJsonSchema<T>
  extends ArrayJsonSchemaExclusive,
    JsonSchemaGeneric<unknown[]> {
  type: "array"
  items: ValueJsonSchema<T>
}

export interface ArrayJsonSchemaExclusive {
  minLength?: number
  maxLength?: number
  uniqueItems?: boolean
}

export interface NumberJsonSchema
  extends NumericJsonSchemaExclusive,
    JsonSchemaGeneric<number> {
  type: "number"
}

export interface IntegerJsonSchema
  extends NumericJsonSchemaExclusive,
    JsonSchemaGeneric<number> {
  type: "integer"
}

export interface NumericJsonSchemaExclusive {
  multipleOf?: number
  minimum?: number
  maximum?: number
  exclusiveMinimum?: number
  exclusiveMaximum?: number
}

export interface StringJsonSchema
  extends StringJsonSchemaExclusive,
    JsonSchemaGeneric<string> {
  type: "string"
}

export interface StringJsonSchemaExclusive {
  minLength?: number
  maxLength?: number
  pattern?: string
  format?:
    | "date-time"
    | "time"
    | "date"
    | "email"
    | "idn-email"
    | "hostname"
    | "idn-hostname"
    | "ipv4"
    | "ipv6"
    | "uri"
    | "uri-reference"
    | "iri"
    | "iri-reference"
    | "uri-template"
    | "json-pointer"
    | "relative-json-pointer"
    | "regex"
}

export interface StringEnumJsonSchema<
  T extends readonly string[] = readonly string[]
> extends JsonSchemaGeneric {
  type?: "string"
  enum: T
}

export interface ConstJsonSchema<T = unknown> extends JsonSchemaGeneric<T> {
  type?: string
  const: T
}

export type RequiredJsonSchema<T> = T extends NeverType
  ? false
  : T extends AnyType
  ? {}
  : T extends NullType
  ? NullJsonSchema
  : T extends BooleanType
  ? BooleanJsonSchema
  : T extends NumberType
  ? NumberJsonSchema
  : T extends IntegerType
  ? IntegerJsonSchema
  : T extends StringType
  ? StringJsonSchema
  : T extends ObjectType<infer O, infer P>
  ? ObjectJsonSchema<O, P>
  : T extends ArrayType<infer C>
  ? ArrayJsonSchema<C>
  : T extends StringEnumType<infer C>
  ? StringEnumJsonSchema<C>
  : T extends ConstType<infer C>
  ? ConstJsonSchema<C>
  : unknown

export type ValueJsonSchema<T> = RequiredJsonSchema<
  T extends OptionalType<BaseSchema, infer C> ? C : T
>

export interface AnyOfJsonSchema extends JsonSchemaGeneric {
  anyOf: unknown[]
}

const GENERIC_JSON_SCHEMA_KEYS = [
  "title",
  "description",
  "default",
  "examples",
] as const

function mergePropertiesToJsonSchema<Common, T = unknown>(
  target: Common & JsonSchemaGeneric<T>,
  subject: Common & JsonSchemaGeneric<T>,
  properties: (keyof Common)[] = []
) {
  for (const property of [...properties, ...GENERIC_JSON_SCHEMA_KEYS]) {
    if ((property as string) in subject) {
      target[property] = subject[property]
    }
  }
  Object.assign(target, subject.raw ?? {})
  return target
}
