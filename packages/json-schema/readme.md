# JSON Schema transform for Schema Kit

## About Schema Kit

Schema Kit allows you to create a meta-schema that can be transformed into
TypeScript, JSON Schema, and more.

## Usage

| Schema Kit           | JSON Schema
| -------------------- | -----------------------------------------
| `never()`            | `false`
| `any()`              | `{}`
| `buildConst(value)`  | `{ const: value }`
| `buildNull()`        | `null`
| `boolean()`          | `{ type: "boolean" }`
| `number()`           | `{ type: "number" }`
| `integer()`          | `{ type: "integer" }`
| `string()`           | `{ type: "string" }`
| `stringEnum(values)` | `{ type: "string", enum: values }`
| `optional(schema)`   | excluded from `required` array
| `object(props)`      | `{ type: "object", properties: { ... } }`
| `array(schema)`      | `{ type: "array", items: schema }`
| `union(schemas)`     | `{ anyOf: schemas }`

Optional nodes only make sense in the context of object node property values.

For `const`, the appropriate `type` will be included if the value is `null`, a boolean, a number, a string, or an array.

```typescript
import { toJsonSchema } from "@schema-kit/json-schema"
```

### toJsonSchema(schema)

Converts the input schema into JSON Schema.

Parameters:
* `schema` - The input schema.

Returns: `false | object` - The output JSON Schema.

## Options

The Schema Kit schema builders can be passed an optional options object to further describe their schema.

Check the JSON Schema documentation for information on how to use these options.

### Common options

* title
* description
* default
* examples
* raw - Merges an object into the schema.

### Numeric: number and integer

* multipleOf
* minimum
* maximum
* exclusiveMinimum
* exclusiveMaximum

### string

* minLength
* maxLength
* pattern
* format

### array

* minLength
* maxLength
* uniqueItems
