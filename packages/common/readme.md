# Common types for Schema Kit

## Usage

```typescript
import {
  boolean,
  integer,
  buildNull,
  number,
  object,
  optional,
  string,
} from "@schema-kit/common"
```

A description of each type can be found on [the wiki](https://gitlab.com/seangenabe/schema-kit/-/wikis/Schema-Kit-types).

### Common options

* title: Optional.
* description: Optional.

### never(opts)

Creates a never node.

### any(opts)

Creates an any node.

### buildConst(value, opts)

Creates a const node.

Parameters:
* value: A constant value.

### buildNull(opts)

Creates a null node.

### boolean(opts)

Creates a boolean node.

### integer(opts)

Creates an integer node. 

### number(opts)

Creates a number node.

### object(properties, opts)

Creates an object node.

Parameters:
* properties: A key-value object containing string keys and their corresponding schema nodes.

### optional(child, opts)

Creates an optional node.

Parameters:
* child: The child schema node. The child node cannot be another optional node.

### array(child, opts)

Creates an array node.

Parameters:
* child: The child schema node.

### union(children, opts)

Creates a union node.

Parameters:
* children: An array of the children schema nodes.
