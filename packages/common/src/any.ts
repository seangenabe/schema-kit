import { CommonBuilderOptions } from "./common"

export function any(opts: AnyBuilderOptions = {}): AnyType {
  return { type: "any", opts }
}

export interface AnyBuilderOptions extends CommonBuilderOptions {}

export interface AnyType {
  readonly type: "any"
  readonly opts: AnyBuilderOptions
}
