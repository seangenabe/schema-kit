import test from "ava"
import { number } from "./number"

test("number", (t) => {
  t.deepEqual(number(), { type: "number", opts: {} })
})
