import { Schema } from "./schema"
import { BaseSchema, CommonBuilderOptions } from "./common"
import { BooleanType } from "./boolean"

export function union<
  O extends BaseSchema = BooleanType,
  T extends Schema<O>[] = Schema<O>[]
>(children: T, opts: UnionBuilderOptions = {}): UnionType<O, T> {
  return { type: "union", children, opts }
}

export interface UnionBuilderOptions extends CommonBuilderOptions {}

export interface UnionType<
  O extends BaseSchema = BooleanType,
  T extends Schema<O>[] = Schema<O>[]
> {
  type: "union"
  children: T
  opts: UnionBuilderOptions
}
