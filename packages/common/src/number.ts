import { CommonBuilderOptions } from "./common"

export function number(opts: NumberBuilderOptions = {}): NumberType {
  return { type: "number", opts }
}

export interface NumberBuilderOptions extends CommonBuilderOptions {}

export interface NumberType {
  readonly type: "number"
  readonly opts: NumberBuilderOptions
}
