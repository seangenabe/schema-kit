import test from "ava"
import { string } from "./string"

test("string", (t) => {
  t.deepEqual(string(), { type: "string", opts: {} })
})
