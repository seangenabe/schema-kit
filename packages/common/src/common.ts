export interface CommonBuilderOptions {
  title?: string
  description?: string
}

export interface BaseSchema<
  Opts extends CommonBuilderOptions = CommonBuilderOptions
> {
  type: string
  opts: Opts
}
