import test from "ava"
import { integer } from "./integer"

test("integer", (t) => {
  t.deepEqual(integer(), { type: "integer", opts: {} })
})
