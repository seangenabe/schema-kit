import { BooleanType } from "./boolean"
import { BaseSchema, CommonBuilderOptions } from "./common"
import { Schema } from "./schema"

export function optional<
  O extends BaseSchema = BooleanType,
  T extends Schema<O> = Schema<O>
>(
  child: T extends OptionalTyped ? never : T,
  opts: OptionalBuilderOptions = {}
): OptionalType<O, T> {
  if (!child) {
    throw new Error("Must provide a child.")
  }
  if (isOptional(child)) {
    throw new Error("Cannot contain an optional node.")
  }
  return { type: "optional", child, opts }
}

export interface OptionalBuilderOptions extends CommonBuilderOptions {}

export interface OptionalType<
  O extends BaseSchema = BooleanType,
  T extends Schema<O> = Schema<O>
> extends OptionalTyped {
  readonly child: T extends OptionalTyped ? never : T
  readonly opts: OptionalBuilderOptions
}

export interface OptionalTyped {
  readonly type: "optional"
}

export function isOptional(
  input: BaseSchema
): input is OptionalTyped & typeof input {
  return input.type === "optional"
}
