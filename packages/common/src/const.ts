import { CommonBuilderOptions } from "./common"

export function buildConst<T = unknown>(
  value: T,
  opts: ConstBuilderOptions = {}
): ConstType<T> {
  return { type: "const", value, opts }
}

export interface ConstBuilderOptions extends CommonBuilderOptions {}

export interface ConstType<T = unknown> {
  readonly type: "const"
  readonly value: T
  readonly opts: ConstBuilderOptions
}
