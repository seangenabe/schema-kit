import { BooleanType } from "./boolean"
import { BaseSchema, CommonBuilderOptions } from "./common"
import { Schema } from "./schema"

export function object<
  O extends BaseSchema = BooleanType,
  T extends Record<string, Schema<O>> = Record<string, Schema<O>>
>(properties: T, opts: ObjectBuilderOptions = {}): ObjectType<O, T> {
  if (typeof properties !== "object") {
    throw new TypeError("properties must be an object")
  }
  return { type: "object", properties, opts }
}

export interface ObjectBuilderOptions extends CommonBuilderOptions {}

export interface ObjectType<
  O extends BaseSchema = BooleanType,
  T extends Record<string, Schema<O>> = Record<string, Schema<O>>
> {
  readonly type: "object"
  readonly properties: T
  readonly opts: ObjectBuilderOptions
}
