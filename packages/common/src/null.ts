import { CommonBuilderOptions } from "./common"

export function buildNull(opts: NullBuilderOptions = {}): NullType {
  return { type: "null", opts }
}

export interface NullBuilderOptions extends CommonBuilderOptions {}

export interface NullType {
  readonly type: "null"
  readonly opts: NullBuilderOptions
}
