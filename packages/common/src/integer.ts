import { NumberBuilderOptions } from "./number"

export function integer(opts: IntegerBuilderOptions = {}): IntegerType {
  return { type: "integer", opts }
}

export interface IntegerBuilderOptions extends NumberBuilderOptions {}

export interface IntegerType {
  readonly type: "integer"
  readonly opts: IntegerBuilderOptions
}
