import { BooleanType } from "./boolean"
import { BaseSchema, CommonBuilderOptions } from "./common"
import { Schema } from "./schema"

export function array<
  O extends BaseSchema = BooleanType,
  T extends Schema<O> = Schema<O>
>(child: T, opts: ArrayBuilderOptions = {}): ArrayType<O, T> {
  return { type: "array", child, opts }
}

export interface ArrayBuilderOptions extends CommonBuilderOptions {}

export interface ArrayType<
  O extends BaseSchema = BooleanType,
  T extends Schema<O> = Schema<O>
> {
  readonly type: "array"
  readonly child: T
  readonly opts: ArrayBuilderOptions
}
