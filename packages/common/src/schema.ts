import { AnyType } from "./any"
import { ArrayType } from "./array"
import { BooleanType } from "./boolean"
import { BaseSchema } from "./common"
import { ConstType } from "./const"
import { IntegerType } from "./integer"
import { NullType } from "./null"
import { NumberType } from "./number"
import { ObjectType } from "./object"
import { OptionalType } from "./optional"
import { StringType } from "./string"
import { StringEnumType } from "./string-enum"
import { UnionType } from "./union"

export type Schema<O extends BaseSchema = BooleanType> =
  | AnyType
  | ArrayType
  | BooleanType
  | ConstType
  | IntegerType
  | NullType
  | NumberType
  | ObjectType
  | OptionalType
  | StringEnumType
  | StringType
  | UnionType
  | O
