import { CommonBuilderOptions } from "./common"

export function never(opts: NeverBuilderOptions = {}): NeverType {
  return { type: "never", opts }
}

export interface NeverBuilderOptions extends CommonBuilderOptions {}

export interface NeverType {
  readonly type: "never"
  readonly opts: NeverBuilderOptions
}
