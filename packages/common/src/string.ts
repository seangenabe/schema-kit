import { CommonBuilderOptions } from "./common"

export function string(opts: StringBuilderOptions = {}): StringType {
  return { type: "string", opts }
}

export interface StringBuilderOptions extends CommonBuilderOptions {}

export interface StringType {
  readonly type: "string"
  readonly opts: StringBuilderOptions
}
