import { CommonBuilderOptions } from "./common"

function stringEnum<InputMembers extends readonly string[] = readonly string[]>(
  values: InputMembers,
  opts?: StringEnumBuilderOptions
): StringEnumType<InputMembers>
function stringEnum<
  InputMembers extends readonly (string | StringEnumMember)[] = readonly (
    | string
    | StringEnumMember
  )[]
>(
  values: InputMembers,
  opts?: StringEnumBuilderOptions
): OutputStringEnumType<InputMembers>
function stringEnum<
  InputMembers extends readonly (string | StringEnumMember)[] = readonly (
    | string
    | StringEnumMember
  )[]
>(
  values: InputMembers,
  opts: StringEnumBuilderOptions = {}
): OutputStringEnumType<InputMembers> {
  return ({
    type: "stringEnum",
    values: values.map((value) => {
      if (typeof value === "string") {
        return { value } as StringEnumMember<typeof value>
      }
      return value
    }),
    opts,
  } as unknown) as OutputStringEnumType<InputMembers>
}
export { stringEnum }

export type OutputStringEnumType<
  InputMembers extends readonly (string | StringEnumMember)[]
> = StringEnumType<
  {
    readonly [key in keyof InputMembers]: InputMembers[key] extends StringEnumMember<
      infer S
    >
      ? S
      : CoerceString<InputMembers[key]>
  }
>

export type CoerceString<T> = T extends string ? T : never

export interface StringEnumBuilderOptions extends CommonBuilderOptions {}

export interface StringEnumType<
  Keys extends readonly string[] = readonly string[]
> {
  readonly type: "stringEnum"
  readonly values: StringEnumMember<Keys[number]>[]
  readonly opts: StringEnumBuilderOptions
}

export interface StringEnumMember<T extends string = string> {
  value: T
  readonly opts?: StringEnumMemberOptions
}

export interface StringEnumMemberOptions {}
