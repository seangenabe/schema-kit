import { CommonBuilderOptions } from "./common"

export function boolean(opts: BooleanBuilderOptions = {}): BooleanType {
  return { type: "boolean", opts }
}

export interface BooleanBuilderOptions extends CommonBuilderOptions {}

export interface BooleanType {
  readonly type: "boolean"
  readonly opts: BooleanBuilderOptions
}
